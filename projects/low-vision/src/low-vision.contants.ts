export enum KeyCodes {
  Tab = 9,
  Enter = 13,
  Space = 32,
}
