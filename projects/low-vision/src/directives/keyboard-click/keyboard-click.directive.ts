import { Directive, HostListener } from '@angular/core';
import { spaceEnterClick } from '../../helpers/keyboard.helper';

/* tslint:disable */
@Directive({
  selector: '[utk-keyboard-click]',
})
/* tslint:enable */
export class KeyboardClickDirective {
  @HostListener('keydown', ['$event'])
  onKeyDown = (e) => spaceEnterClick(e);
}
