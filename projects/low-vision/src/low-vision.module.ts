import { NgModule } from '@angular/core';
import { KeyboardClickDirective } from './directives/keyboard-click/keyboard-click.directive';

@NgModule({
  declarations: [
    KeyboardClickDirective,
  ],
  imports: [
  ],
  exports: [
    KeyboardClickDirective
  ]
})
export class LowVisionModule { }
