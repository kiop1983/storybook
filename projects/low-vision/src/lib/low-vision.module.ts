import { NgModule } from '@angular/core';
import { LowVisionComponent } from './low-vision.component';



@NgModule({
  declarations: [
    LowVisionComponent
  ],
  imports: [
  ],
  exports: [
    LowVisionComponent
  ]
})
export class LowVisionModule { }
