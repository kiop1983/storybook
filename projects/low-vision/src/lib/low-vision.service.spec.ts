import { TestBed } from '@angular/core/testing';

import { LowVisionService } from './low-vision.service';

describe('LowVisionService', () => {
  let service: LowVisionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LowVisionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
