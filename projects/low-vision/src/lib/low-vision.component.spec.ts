import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LowVisionComponent } from './low-vision.component';

describe('LowVisionComponent', () => {
  let component: LowVisionComponent;
  let fixture: ComponentFixture<LowVisionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LowVisionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LowVisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
