import { KeyCodes } from '../low-vision.contants';
import { fromEvent, Subscription } from 'rxjs';


// Данный интерфейс служит для "обмана" линтера дабы указанные сойства не были устаревшими
interface KeyboardEventIE extends KeyboardEvent {
  keyCode: number;
  which: number;
}

// IE fix, don't use 'key'
export const getKeyCode = (e: KeyboardEventIE): number => e.keyCode || e.which;

export const isEnter = (e: KeyboardEvent): boolean => {
  return getKeyCode(e) === KeyCodes.Enter;
};
export const isSpace = (e: KeyboardEvent): boolean => {
  return getKeyCode(e) === KeyCodes.Space;
};

export const isTab = (e: KeyboardEvent): boolean => {
  return getKeyCode(e) === KeyCodes.Tab;
};

export const isSpaceOrEnter = (e: KeyboardEvent): boolean => {
  return isEnter(e) || isSpace(e);
};

export const spaceEnterClick = (event: KeyboardEvent): void => {
  if (!isSpaceOrEnter(event)) {
    return;
  }
  const element = event.target as HTMLElement;
  const clickSubscription: Subscription = fromEvent(element, 'click').subscribe((e) => {
    e.stopPropagation();
    clickSubscription?.unsubscribe();
  });
  element.click();
  event.preventDefault();
  event.stopPropagation();
};
