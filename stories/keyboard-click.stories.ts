import { CommonModule } from '@angular/common';
import { Story, Meta, moduleMetadata } from '@storybook/angular';
import { KeyboardClickDirective, LowVisionModule } from 'projects/low-vision/src/public-api';


export default {
  title: 'Example/Keyboard click',
  component: KeyboardClickDirective,
  decorators: [
    moduleMetadata({
      imports: [CommonModule, LowVisionModule],
    }),
  ],
} as Meta;

const Template: Story<KeyboardClickDirective> = (args: KeyboardClickDirective) => ({
  props: args,
  template: `
    <a href="javascript:alert(1);" tabindex="0" utk-keyboard-click>click able</a><br/>
    <a href="javascript:alert(2);" tabindex="0">semiclick able</a>
  `
});

export const Primary = Template.bind({});
